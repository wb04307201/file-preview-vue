# 文件预览 vue 示例

## 前端

```bash
# 安装依赖库
yarn

# 启动
npm run dev
```
![img.png](img.png)

## 后端
```bash
# 安装minio
docker run -p 9000:9000 -p 9090:9090 --name minio1 -v D:\minio\data:/data -e "MINIO_ROOT_USER=ROOTUSER" -e "MINIO_ROOT_PASSWORD=CHANGEME123" quay.io/minio/minio server /data --console-address ":9090"
# 再minio里配置access-key，access-key
# 安装onlyoffice
docker run --name onlyoffice -i -t -d -p 80:80 -e JWT_ENABLED=false onlyoffice/documentserver
# 调整配置文件，启动后端服务
```
