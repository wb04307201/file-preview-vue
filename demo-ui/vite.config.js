import {fileURLToPath, URL} from 'node:url'

import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [vue(),], resolve: {
        alias: {
            '@': fileURLToPath(new URL('./src', import.meta.url))
        }
    }, server: {
        //用来配置跨域
        host: 'localhost', port: 3000, proxy: {
            // 调用重写接口的代理
            '/api': {
                target: 'http://localhost:8090',//目标服务器地址
                changeOrigin: true, rewrite: (path) => path.replace(/^\/api/, '')
            },
            // 调用jar集成接口的代理
            '/file/preview': {
                target: 'http://localhost:8090',//目标服务器地址
                changeOrigin: true,
            },
        }
    }
})
