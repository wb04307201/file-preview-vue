package cn.wubo.file.preview.demo;

import cn.wubo.file.preview.core.FilePreviewInfo;
import cn.wubo.file.preview.core.FilePreviewService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(value = "/demo")
public class DemoController {

    @Resource
    FilePreviewService filePreviewService;

    @PostMapping(value = "/list")
    public Result<List<FilePreviewInfo>> upload(@RequestBody FilePreviewInfo filePreviewInfo) {
        return Result.success(filePreviewService.list(filePreviewInfo));
    }

    @PostMapping(value = "/delete")
    public Result<Boolean> delete(@RequestBody FilePreviewInfo filePreviewInfo) {
        return Result.success(filePreviewService.delete(filePreviewInfo.getId()));
    }

    @PostMapping(value = "/upload")
    public Result<FilePreviewInfo> upload(MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                return Result.success(filePreviewService.covert(file.getInputStream(), file.getOriginalFilename()));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        throw new RuntimeException("请选择文件！");
    }
}
