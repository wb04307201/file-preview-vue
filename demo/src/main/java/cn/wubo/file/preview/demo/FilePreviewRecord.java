package cn.wubo.file.preview.demo;

import cn.wubo.file.preview.core.FilePreviewInfo;
import cn.wubo.sql.util.annotations.Column;
import cn.wubo.sql.util.annotations.Condition;
import cn.wubo.sql.util.annotations.Key;
import cn.wubo.sql.util.annotations.Table;
import cn.wubo.sql.util.enums.ColumnType;
import cn.wubo.sql.util.enums.GenerationType;
import cn.wubo.sql.util.enums.StatementCondition;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(value = "file_preview_record", desc = "文件预览记录")
public class FilePreviewRecord {
    @Key(value = GenerationType.DB)
    @Column(value = "id")
    private String id;
    @Condition(value = StatementCondition.LIKE)
    @Column(value = "original_filename", desc = "原文件名", type = ColumnType.VARCHAR)
    private String originalFilename;
    @Condition(value = StatementCondition.LIKE)
    @Column(value = "file_name", desc = "文件名", type = ColumnType.VARCHAR)
    private String fileName;
    @Column(value = "file_path", desc = "文件路径", type = ColumnType.VARCHAR)
    private String filePath;
    @Column(value = "create_time", desc = "创建时间", type = ColumnType.DATE)
    private Date createTime;

    public static FilePreviewRecord trans(FilePreviewInfo filePreviewInfo) {
        return new FilePreviewRecord(filePreviewInfo.getId(), filePreviewInfo.getOriginalFilename(), filePreviewInfo.getFileName(), filePreviewInfo.getFilePath(), filePreviewInfo.getCreateTime());
    }

    public FilePreviewInfo getFilePreviewInfo() {
        return new FilePreviewInfo(this.id, this.originalFilename, this.fileName, this.filePath, this.createTime);
    }
}
