package cn.wubo.file.preview.demo;

import cn.wubo.file.storage.core.FileInfo;
import cn.wubo.sql.util.annotations.Column;
import cn.wubo.sql.util.annotations.Condition;
import cn.wubo.sql.util.annotations.Key;
import cn.wubo.sql.util.annotations.Table;
import cn.wubo.sql.util.enums.ColumnType;
import cn.wubo.sql.util.enums.GenerationType;
import cn.wubo.sql.util.enums.StatementCondition;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(value = "file_storage_record", desc = "文件存储记录")
public class FileStorageRecord {
    @Key(value = GenerationType.DB)
    @Column(value = "id")
    private String id;
    @Column(value = "size", desc = "大小", type = ColumnType.NUMBER)
    private Long size;
    @Condition(value = StatementCondition.LIKE)
    @Column(value = "file_name", desc = "文件名", type = ColumnType.VARCHAR)
    private String filename;
    @Condition(value = StatementCondition.LIKE)
    @Column(value = "original_filename", desc = "原文件名", type = ColumnType.VARCHAR)
    private String originalFilename;
    @Column(value = "base_path", desc = "基础路径", type = ColumnType.VARCHAR)
    private String basePath;
    @Column(value = "path", desc = "路径", type = ColumnType.VARCHAR)
    private String path;
    @Column(value = "content_type", desc = "content-type", type = ColumnType.VARCHAR)
    private String contentType;
    @Column(value = "platform", desc = "平台", type = ColumnType.VARCHAR)
    protected String platform;
    @Condition(value = StatementCondition.LIKE)
    @Column(value = "alias", desc = "别名", type = ColumnType.VARCHAR)
    private String alias;
    @Column(value = "create_time", desc = "创建时间", type = ColumnType.DATE)
    private Date createTime;

    public static FileStorageRecord trans(FileInfo fileInfo) {
        return new FileStorageRecord(fileInfo.getId(), fileInfo.getSize(), fileInfo.getFilename(), fileInfo.getOriginalFilename(), fileInfo.getBasePath(), fileInfo.getPath(), fileInfo.getContentType(), fileInfo.getPlatform(), fileInfo.getAlias(), fileInfo.getCreateTime());
    }

    public FileInfo getFileInfo() {
        FileInfo fileInfo = new FileInfo();
        fileInfo.setId(this.id);
        fileInfo.setSize(this.size);
        fileInfo.setFilename(this.filename);
        fileInfo.setOriginalFilename(this.originalFilename);
        fileInfo.setBasePath(this.basePath);
        fileInfo.setPath(this.path);
        fileInfo.setContentType(this.contentType);
        fileInfo.setPlatform(this.platform);
        fileInfo.setAlias(this.alias);
        fileInfo.setCreateTime(this.createTime);
        return fileInfo;
    }
}
