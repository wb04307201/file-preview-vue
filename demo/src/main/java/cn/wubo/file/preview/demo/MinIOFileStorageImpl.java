package cn.wubo.file.preview.demo;

import cn.wubo.file.preview.core.FilePreviewInfo;
import cn.wubo.file.preview.storage.IFileStorage;
import cn.wubo.file.storage.core.FileInfo;
import cn.wubo.file.storage.core.FileStorageService;
import cn.wubo.file.storage.core.MultipartFileStorage;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

@Component
public class MinIOFileStorageImpl implements IFileStorage {

    @Resource
    FileStorageService fileStorageService;

    @Override
    public FilePreviewInfo save(byte[] bytes, String fileName) {
        FileInfo fileInfo = fileStorageService.save(new MultipartFileStorage(fileName, null, null, bytes).setAlias("minio-1").setPath("preview"));
        FilePreviewInfo filePreviewInfo = new FilePreviewInfo();
        filePreviewInfo.setFileName(fileInfo.getOriginalFilename());
        filePreviewInfo.setFilePath(fileInfo.getId());
        return filePreviewInfo;
    }

    @Override
    public Boolean delete(FilePreviewInfo filePreviewInfo) {
        return fileStorageService.delete(filePreviewInfo.getFilePath());
    }

    @Override
    public byte[] getBytes(FilePreviewInfo filePreviewInfo) {
        MultipartFileStorage file = fileStorageService.download(filePreviewInfo.getFilePath());
        return file.getBytes();
    }

    @Override
    public void init() {

    }
}
