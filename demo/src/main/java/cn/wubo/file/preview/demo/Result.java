package cn.wubo.file.preview.demo;

import lombok.Data;

@Data
public class Result<T> {
    private Long code;
    private String msg;
    private Long count;
    private T data;

    public Result() {
    }

    public Result(Long count, T data) {
        this.code = 0L;
        this.msg = "";
        this.count = count;
        this.data = data;
    }

    public Result(T data) {
        this.code = 0L;
        this.msg = "";
        this.data = data;
    }

    public static <T> Result<T> success(Long count, T data) {
        return new Result<>(count, data);
    }

    public static <T> Result<T> success(T data) {
        return new Result<>(data);
    }

    public static <T> Result<T> fail(Exception e) {
        Result<T> result = new Result<>();
        result.setCode(500L);
        result.setMsg(e.getMessage());
        return result;
    }

}
